;;; Guile-SemVer
;;; Copyright 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Guile-SemVer.
;;;
;;; Guile-SemVer is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-SemVer is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-SemVer.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (semver)
             (semver ranges)
             (srfi srfi-64))

(test-begin "semver ranges")


;;; Identity

(let ((svr (semver-range `((= ,(make-semver 1))))))
  (test-assert "=1.0.0 includes 1.0.0"
    (semver-range-contains? svr (make-semver 1)))

  (test-assert "=1.0.0 excludes 2.0.0"
    (not (semver-range-contains? svr (make-semver 2)))))


;;; Bounds

(let ((svr (semver-range `((>= ,(make-semver 0 7 5)) (< ,(make-semver 1))))))
  (test-assert ">= 0.7.5 <1.0.0 includes 0.7.5"
    (semver-range-contains? svr (make-semver 0 7 5)))

  (test-assert ">= 0.7.5 <1.0.0 includes 0.9.9"
    (semver-range-contains? svr (make-semver 0 9 9)))

  (test-assert ">= 0.7.5 <1.0.0 exlucdes 1.0.0-rc"
    (not (semver-range-contains? svr (make-semver 1 #:pre '("rc")))))

  (test-assert ">= 0.7.5 <1.0.0 excludes 0.7.0"
    (not (semver-range-contains? svr (make-semver 0 7))))

  (test-assert ">= 0.7.5 <1.0.0 exludes 1.0.0"
    (not (semver-range-contains? svr (make-semver 1))))

  (test-assert ">= 0.7.5 <1.0.0 exludes 2.0.0"
    (not (semver-range-contains? svr (make-semver 2)))))


;;; X-Ranges

(let ((svr (semver-range `((= ,(semver-slice 1))))))
  (test-assert "1.x includes 1.5.2"
    (semver-range-contains? svr (make-semver 1 5 2)))

  (test-assert "1.x includes 1.0.0"
    (semver-range-contains? svr (make-semver 1)))

  (test-assert "1.x excludes 0.9.9"
    (not (semver-range-contains? svr (make-semver 0 9 9))))

  (test-assert "1.x excludes 2.0.0"
    (not (semver-range-contains? svr (make-semver 2)))))


;;; Tildes

(let ((svr (semver-range `((~ ,(make-semver 1 2 3))))))
  (test-assert "~1.2.3 includes 1.2.3"
    (semver-range-contains? svr (make-semver 1 2 3)))

  (test-assert "~1.2.3 includes 1.2.9"
    (semver-range-contains? svr (make-semver 1 2 9)))

  (test-assert "~1.2.3 excludes 1.3.0"
    (not (semver-range-contains? svr (make-semver 1 3 0))))

  (test-assert "~1.2.3 excludes 1.2.2"
    (not (semver-range-contains? svr (make-semver 1 2 2)))))


;;; Carets

(let ((svr (semver-range `((^ ,(make-semver 1 2 3))))))
  (test-assert "^1.2.3 includes 1.2.3"
    (semver-range-contains? svr (make-semver 1 2 3)))

  (test-assert "^1.2.3 includes 1.3.0"
    (semver-range-contains? svr (make-semver 1 3 0)))

  (test-assert "^1.2.3 excludes 2.0.0"
    (not (semver-range-contains? svr (make-semver 2))))

  (test-assert "^1.2.3 excludes 1.2.2"
    (not (semver-range-contains? svr (make-semver 1 2 2)))))

(let ((svr (semver-range `((^ ,(make-semver 0 2 3))))))
  (test-assert "^0.2.3 includes 0.2.3"
    (semver-range-contains? svr (make-semver 0 2 3)))

  (test-assert "^0.2.3 includes 0.2.4"
    (semver-range-contains? svr (make-semver 0 2 4)))

  (test-assert "^0.2.3 excludes 0.3.0"
    (not (semver-range-contains? svr (make-semver 0 3))))

  (test-assert "^0.2.3 excludes 0.2.2"
    (not (semver-range-contains? svr (make-semver 0 2 2)))))

(let ((svr (semver-range `((^ ,(make-semver 0 0 3))))))
  (test-assert "^0.0.3 includes 0.0.3"
    (semver-range-contains? svr (make-semver 0 0 3)))

  (test-assert "^0.0.3 excludes 0.0.4"
    (not (semver-range-contains? svr (make-semver 0 0 4))))

  (test-assert "^0.0.3 excludes 0.0.2"
    (not (semver-range-contains? svr (make-semver 0 0 2)))))

(let ((svr (semver-range `((^ ,(semver-slice 1 2))))))
  (test-assert "^1.2 includes 1.5.0"
    (semver-range-contains? svr (make-semver 1 5 0))))

(let ((svr (semver-range `((^ ,(make-semver 0 0 0))))))
  (test-assert "^0.0.0 includes 0.0.0"
    (semver-range-contains? svr (make-semver 0 0 0)))

  (test-assert "^0.0.0 excludes 0.0.1"
    (not (semver-range-contains? svr (make-semver 0 0 1)))))

(let ((svr (semver-range `((^ ,(semver-slice 0 0))))))
  (test-assert "^0.0 includes 0.0.5"
    (semver-range-contains? svr (make-semver 0 0 5)))

  (test-assert "^0.0 excludes 0.1.0"
    (not (semver-range-contains? svr (make-semver 0 1 0)))))

(let ((svr (semver-range `((^ ,(semver-slice 0))))))
  (test-assert "^0.0 includes 0.5.0"
    (semver-range-contains? svr (make-semver 0 5 0)))

  (test-assert "^0.0 excludes 1.0.0"
    (not (semver-range-contains? svr (make-semver 1 0 0)))))


;;; Hyphens

(let ((svr (semver-range `((- ,(make-semver 1) ,(make-semver 1 5))))))
  (test-assert "1.0.0 - 1.5.0 includes 1.2.5"
    (semver-range-contains? svr (make-semver 1 2 5)))

  (test-assert "1.0.0 - 1.5.0 excludes 0.5.0"
    (not (semver-range-contains? svr (make-semver 0 5))))

  (test-assert "1.0.0 - 1.5.0 excludes 2.0.0"
    (not (semver-range-contains? svr (make-semver 2))))

  (test-assert "1.0.0 - 1.5.0 includes 1.0.0"
    (semver-range-contains? svr (make-semver 1)))

  (test-assert "1.0.0 - 1.5.0 includes 1.5.0"
    (semver-range-contains? svr (make-semver 1 5))))


;;; Unions

(let ((svr (semver-range `(,(make-semver 1)) `(,(make-semver 2)))))
  (test-assert "1.0.0 || 2.0.0 includes 1.0.0"
    (semver-range-contains? svr (make-semver 1)))

  (test-assert "1.0.0 || 2.0.0 includes 2.0.0"
    (semver-range-contains? svr (make-semver 2)))

  (test-assert "1.0.0 || 2.0.0 excludes 1.5.0"
    (not (semver-range-contains? svr (make-semver 1 5)))))


;;; Constants

(test-assert "Any SemVer contains 1.2.3"
  (semver-range-contains? *semver-range-any* (make-semver 1 2 3)))

(test-assert "Empty SemVer range excludes 1.2.3"
  (not (semver-range-contains? *semver-range-empty* (make-semver 1 2 3))))


;;; Prerelease tags

(let ((svr (semver-range `((> ,(make-semver 1 2 3 #:pre '("alpha" 3)))))))
  (test-assert ">1.2.3-alpha.3 includes 1.2.3-alpha.7"
    (semver-range-contains? svr (make-semver 1 2 3 #:pre '("alpha" 7))))

  (test-assert ">1.2.3-alpha.3 excludes 3.4.5-alpha.9"
    (not (semver-range-contains? svr (make-semver 3 4 5 #:pre '("alpha" 9))))))


;;; Reading

(test-equal "Reads wlidcard"
  *semver-range-any*
  (string->semver-range "*"))

(test-equal "Reads empty"
  *semver-range-any*
  (string->semver-range ""))

(test-equal "Reads tag parts with leading zeros as strings"
  (semver-range `(,(make-semver 1 2 3 #:pre '("rc" "01"))))
  (string->semver-range "1.2.3-rc.01"))

(test-assert "Allows ~>"
  (string->semver-range "~>1.2.3"))

(test-assert "Reads mixed operators"
  (string->semver-range "> <1.2.3"))

(test-assert "Give preference to last operator"
  (let ((svr< (string->semver-range "> <1.2.3"))
        (svr> (string->semver-range "< >1.2.3")))
    (and (semver-range-contains? svr< (make-semver 1))
         (semver-range-contains? svr> (make-semver 2))
         (not (semver-range-contains? svr< (make-semver 2)))
         (not (semver-range-contains? svr> (make-semver 1))))))

(test-assert "Prefers ~ among mixed operators"
  (let ((svr (string->semver-range "< ~ >1.2.3")))
    (semver-range-contains? svr (make-semver 1 2 3))))

(test-assert "Prefers ^ among mixed operators"
  (let ((svr (string->semver-range "< ^ >1.2.3")))
    (semver-range-contains? svr (make-semver 1 2 3))))

(test-assert "Does not read ~ and ^ mixed together"
  (not (string->semver-range "~ ^1.2.3")))

(test-assert "Allows multiple spaces"
  (string->semver-range "<=1.0.0   <1.2.2"))

(test-assert "Allows commas mixed with spaces"
  (string->semver-range "<=1.0.0, <1.2.2"))

(test-end "semver ranges")
