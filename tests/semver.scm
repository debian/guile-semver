;;; Guile-SemVer
;;; Copyright 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Guile-SemVer.
;;;
;;; Guile-SemVer is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-SemVer is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-SemVer.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (semver)
             (ice-9 format)
             (ice-9 match)
             (srfi srfi-1)
             (srfi srfi-64))

(test-begin "semver")

(test-equal "Makes strings"
  (let ((semver (make-semver 1 2 11 #:pre '("alpha" 1) #:build '(3))))
    (semver->string semver))
  "1.2.11-alpha.1+3")

(define *versions*
  (map (lambda (version)
         (cons (car version) (apply make-semver (cdr version))))
       '(("1.0.0-alpha" . (1 0 0 #:pre ("alpha")))
         ("1.0.0-alpha.1" . (1 0 0 #:pre ("alpha" 1)))
         ("1.0.0-alpha.beta" . (1 0 0 #:pre ("alpha" "beta")))
         ("1.0.0-beta" . (1 0 0 #:pre ("beta")))
         ("1.0.0-beta.2" . (1 0 0 #:pre ("beta" 2)))
         ("1.0.0-beta.11" . (1 0 0 #:pre ("beta" 11)))
         ("1.0.0-rc.1" . (1 0 0 #:pre ("rc" 1)))
         ("1.0.0" . (1 0 0))
         ("2.0.0" . (2 0 0))
         ("2.1.0" . (2 1 0))
         ("2.1.1" . (2 1 1)))))

(for-each (match-lambda
            ((a-str b-str a b)
             (test-assert (format #f "~a < ~a" a-str b-str)
               (semver<? a b))
             (test-assert (format #f "~a >= ~a" b-str a-str)
               (not (semver<? b a)))))
          (zip (map car *versions*)
               (cdr (map car *versions*))
               (map cdr *versions*)
               (cdr (map cdr *versions*))))

(test-end)
