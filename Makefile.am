# Guile-SemVer
#
# This file is part of Guile-SemVer.
#
# Guile-SemVer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Guile-SemVer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Guile-SemVer.  If not, see <https://www.gnu.org/licenses/>.

include $(top_srcdir)/build-aux/guile.am

SOURCES =                                       \
  semver.scm                                    \
  semver/partial.scm                            \
  semver/ranges.scm

AM_TESTS_ENVIRONMENT = . $(top_builddir)/build-aux/tests-env;
TEST_EXTENSIONS = .scm
SCM_LOG_DRIVER = $(GUILE) --no-auto-compile \
                 $(top_srcdir)/build-aux/test-driver.scm

TESTS =                                         \
  tests/semver.scm                              \
  tests/semver/ranges.scm

EXTRA_DIST +=                                   \
  $(TESTS)                                      \
  COPYING.CC0                                   \
  bootstrap                                     \
  guix.scm

dist-hook: gen-ChangeLog

.PHONY: gen-ChangeLog
gen-ChangeLog:
	$(AM_V_GEN)if test -d .git; then \
	    { $(top_srcdir)/build-aux/gitlog-to-changelog \
	          --format='%s%n%n%b%n' -- v0.1.0.. && \
	      echo && \
	      tail +8 $(top_srcdir)/ChangeLog; \
	    } > $(distdir)/ChangeLog-t && \
	    { rm -f $(distdir)/ChangeLog && \
	      mv $(distdir)/ChangeLog-t $(distdir)/ChangeLog; } \
	fi
